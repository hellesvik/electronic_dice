$fn=100;

module body(){
    difference(){
        cube([21,21,18]);
        translate([1,1,6]) cube([19,19,19]);
    }
}
module body_tmp(){
    difference(){
        cube([21,21,1.5]);
        translate([1,1,1]) cube([19,19,19]);
    }
}

module pogo(){
    cube([4.5,2.5,4]);
    translate([4.5/2-1.27,2.5/2,-3]) cylinder(d=1,h=3);
    translate([4.5/2,2.5/2,-3]) cylinder(d=1,h=3);
    translate([4.5/2+1.27,2.5/2,-3]) cylinder(d=1,h=3);
    
    translate([4.5/2-1.27,2.5/2,4]) cylinder(d=0.889,h=1.5);
    translate([4.5/2,2.5/2,4]) cylinder(d=0.889,h=1.5);
    translate([4.5/2+1.27,2.5/2,4]) cylinder(d=0.889,h=1.5);
    
    translate([4.5/2-1.27,2.5/2,4]) cylinder(d=0.4826,h=3);
    translate([4.5/2,2.5/2,4]) cylinder(d=0.4826,h=3);
    translate([4.5/2+1.27,2.5/2,4]) cylinder(d=0.4826,h=3);
}
module pogo_space_top(){
    cube([4.5,2.5,7]);
    translate([4.5/2-1.27,2.5/2,-3]) cylinder(d=1.3,h=3);
    translate([4.5/2,2.5/2,-3]) cylinder(d=1.3,h=3);
    translate([4.5/2+1.27,2.5/2,-3]) cylinder(d=1.3,h=3);
}
difference(){
    body();
    translate([1,1,1])
    translate([-(4.5/2),-2.5/2,0])
    translate([10,6,0])
    pogo();
}