DICE_SIDE = 16;
module side(){
    cube([DICE_SIDE,DICE_SIDE,1]);
}

WALLS_THICKNESS = 1.2;
WALLS_EXTRA = 1;
module walls(){
    walls_size = DICE_SIDE+WALLS_THICKNESS + WALLS_EXTRA;
    difference(){
        cube(walls_size);
        translate([WALLS_THICKNESS,WALLS_THICKNESS,WALLS_THICKNESS]) cube(walls_size*2);
    }
}

BOX_CUT = 4*2;
HOLE_DEPTH = 3;
HOLE_SIDE = 11;
module box_cut_bar(){
    cube([DICE_SIDE,BOX_CUT,BOX_CUT], center=true);
}

CORNER_SIDE = 3;
X=1;
Y=2;
Z=3;
module hole_cut_cube(axis){
    difference(){
        cube(HOLE_SIDE);
        if(axis == X){
            cube([CORNER_SIDE, CORNER_SIDE ,DICE_SIDE]);
        }
        if(axis == Y){
            cube([DICE_SIDE, CORNER_SIDE ,CORNER_SIDE]);
        }
        if(axis == Z){
            cube([CORNER_SIDE, DICE_SIDE ,CORNER_SIDE]);
        }
    }
}
module box(){
    difference(){
        cube(DICE_SIDE);
        translate([DICE_SIDE/2,DICE_SIDE,DICE_SIDE]) rotate([0,0,0]) box_cut_bar();
        translate([DICE_SIDE,DICE_SIDE/2,DICE_SIDE]) rotate([0,0,90]) box_cut_bar();
        translate([DICE_SIDE,DICE_SIDE,DICE_SIDE/2]) rotate([0,90,0]) box_cut_bar();
    translate([0,0,DICE_SIDE-HOLE_DEPTH])  hole_cut_cube(X);
    translate([DICE_SIDE-HOLE_DEPTH,0,0])  hole_cut_cube(Y);
    translate([0,DICE_SIDE-HOLE_DEPTH,0])  hole_cut_cube(Z);
    }
}
walls();
translate([WALLS_THICKNESS, WALLS_THICKNESS, WALLS_THICKNESS]) box();
