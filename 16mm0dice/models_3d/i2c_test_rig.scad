
$fn=100;
POGO_D=0.75;
POGO_H=10;
CUBE_D=5;
CUBE_H=3;

SLOPE_SINK=6.7;

module hole_test(){
  difference(){
    cube([CUBE_D,CUBE_D,CUBE_H],center=true);
    cylinder(d=POGO_D,h=POGO_H,center=true);
  }
}

PADDING=0.1;
DICE_SIDE=16+PADDING;

RIG_WALL=2.5;

RIG_L=DICE_SIDE+RIG_WALL*2;
RIG_W=DICE_SIDE/2+RIG_WALL*2;

POINT_SPACE=1.410;
POINT_LIST=[1,2,3,4,11];

module pogo_hole(){
  translate([0,-1,0])
    rotate([-90,0,0])
    cylinder(d=POGO_D,h=POGO_H*2);
}

module pogo_line(){
    color("silver")
      translate([-PCB_THICK/2,0,PCB_THICK/2]){
        for(i = POINT_LIST) {
          translate([i*POINT_SPACE,0,0])
            pogo_hole();
        }
      }
}

BOTTOM_THICK=1;
RIG_THICK=3;
PCB_THICK=1;
SLOPE_OFFSET=1;
CUT_DISTANCE=19.4;

module i2c_test_rig(){
  difference(){
    cube([RIG_L,RIG_W,RIG_THICK]);
    translate([RIG_WALL,RIG_WALL,BOTTOM_THICK])
      union(){
        cube([DICE_SIDE,DICE_SIDE,10]);
        translate([PADDING,-10,0])pogo_line();
    }
  }
}

i2c_test_rig();
