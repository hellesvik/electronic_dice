$fn = 100;
DICE_SIDE=16;
PADDING=1;
PIT_SIDE=DICE_SIDE+PADDING;
PILLAR_D=3;
PILLAR_HOLE_D=PILLAR_D + 0.4;
PILLAR_EXTRA=6;
PILLAR_H=DICE_SIDE+PILLAR_EXTRA;
THICK=6.7;
POGO_THICK=0.5;
POGO_OFFSET_X=8.63-4.4/2+0.3;
POGO_OFFSET_Y=4.65-2.3/2+0.8;
PLATE_EXTRA=3;
PLATE_SIDE=PIT_SIDE+PILLAR_D*2+PLATE_EXTRA;
PIT_OFFSET=(PLATE_SIDE-PIT_SIDE)/2;
PIT_DEPTH=2;
PIT_Z_TRANS=THICK-PIT_DEPTH;
HOLE_PAD=0.2;
LID_EXTRA=2;
LID_SIDE=PLATE_SIDE+LID_EXTRA;
LID_THICK=2;
SLOPE_SINK=6.0;


module pogo_space_top(){
    cube([4.4,2.3,7]);
    translate([4.5/2-1.27,2.5/2,-3]) cylinder(d=1.3,h=3);
    translate([4.5/2,2.5/2,-3]) cylinder(d=1.3,h=3);
    translate([4.5/2+1.27,2.5/2,-3]) cylinder(d=1.3,h=3);
}

module pogo(){
    cube([4.4,2.4,4]);
    translate([4.5/2-1.27,2.5/2,-3]) cylinder(d=1,h=3);
    translate([4.5/2,2.5/2,-3]) cylinder(d=1,h=3);
    translate([4.5/2+1.27,2.5/2,-3]) cylinder(d=1,h=3);
    
    translate([4.5/2-1.27,2.5/2,4]) cylinder(d=0.889,h=1.5);
    translate([4.5/2,2.5/2,4]) cylinder(d=0.889,h=1.5);
    translate([4.5/2+1.27,2.5/2,4]) cylinder(d=0.889,h=1.5);
    
    translate([4.5/2-1.27,2.5/2,4]) cylinder(d=0.4826,h=3);
    translate([4.5/2,2.5/2,4]) cylinder(d=0.4826,h=3);
    translate([4.5/2+1.27,2.5/2,4]) cylinder(d=0.4826,h=3);
}


module rounded_box(drop){
translate([PIT_SIDE/2,PIT_SIDE/2,THICK/2])
  difference(){
    cube([PIT_SIDE,PIT_SIDE,THICK], true);
    color("blue")  translate([0,PIT_SIDE/2,-drop]) rotate([45,0,0]) cube([PIT_SIDE,PIT_SIDE,THICK],true);
    color("green")  translate([0,-PIT_SIDE/2,-drop]) rotate([-45,0,0]) cube([PIT_SIDE,PIT_SIDE,THICK],true);
    color("red")  translate([PIT_SIDE/2,0,-drop]) rotate([0,-45,0]) cube([PIT_SIDE,PIT_SIDE,THICK],true);
    color("pink")  translate([-PIT_SIDE/2,0,-drop]) rotate([0,45,0]) cube([PIT_SIDE,PIT_SIDE,THICK],true);
  }
}

module bottom_plate(){
    difference(){
        cube([PLATE_SIDE,PLATE_SIDE,THICK]);
        translate([PIT_OFFSET,PIT_OFFSET,PIT_Z_TRANS]) rounded_box(SLOPE_SINK);

    translate([PIT_OFFSET,PIT_OFFSET,0])
    translate([POGO_OFFSET_X,POGO_OFFSET_Y,POGO_THICK]) pogo_space_top();
    }

}

module pillars(){
    PILLAR_OFFSET_SHORT=PILLAR_D/2+PLATE_EXTRA/2;
    PILLAR_OFFSET_LONG=PILLAR_OFFSET_SHORT+PLATE_SIDE-PILLAR_D - PLATE_EXTRA;
    translate([PILLAR_OFFSET_SHORT,PILLAR_OFFSET_SHORT,0]) cylinder(d=PILLAR_D,h=PILLAR_H);
    translate([PILLAR_OFFSET_SHORT,PILLAR_OFFSET_LONG,0]) cylinder(d=PILLAR_D,h=PILLAR_H);
    translate([PILLAR_OFFSET_LONG,PILLAR_OFFSET_SHORT,0]) cylinder(d=PILLAR_D,h=PILLAR_H);
    translate([PILLAR_OFFSET_LONG,PILLAR_OFFSET_LONG,0]) cylinder(d=PILLAR_D,h=PILLAR_H);
}

module pillars_holes(){
    PILLAR_OFFSET_SHORT=PILLAR_HOLE_D/2+PLATE_EXTRA/2;
    PILLAR_OFFSET_LONG=PILLAR_OFFSET_SHORT+PLATE_SIDE-PILLAR_HOLE_D - PLATE_EXTRA;
    translate([PILLAR_OFFSET_SHORT,PILLAR_OFFSET_SHORT,0]) cylinder(d=PILLAR_HOLE_D,h=PILLAR_H);
    translate([PILLAR_OFFSET_SHORT,PILLAR_OFFSET_LONG,0]) cylinder(d=PILLAR_HOLE_D,h=PILLAR_H);
    translate([PILLAR_OFFSET_LONG,PILLAR_OFFSET_SHORT,0]) cylinder(d=PILLAR_HOLE_D,h=PILLAR_H);
    translate([PILLAR_OFFSET_LONG,PILLAR_OFFSET_LONG,0]) cylinder(d=PILLAR_HOLE_D,h=PILLAR_H);
}

module lid(){
    
    difference(){
        cube([PLATE_SIDE,PLATE_SIDE,LID_THICK]);
        translate([0,0,-10]) pillars_holes();
    }
}

module plate_with_holes(){
    difference(){
        bottom_plate();
        pillars();
    }
}

module rounded_box(drop){
translate([PIT_SIDE/2,PIT_SIDE/2,THICK/2])
  difference(){
    cube([PIT_SIDE,PIT_SIDE,THICK], true);
    color("blue")  translate([0,PIT_SIDE/2,-drop]) rotate([45,0,0]) cube([PIT_SIDE,PIT_SIDE,THICK],true);
    color("green")  translate([0,-PIT_SIDE/2,-drop]) rotate([-45,0,0]) cube([PIT_SIDE,PIT_SIDE,THICK],true);
    color("red")  translate([PIT_SIDE/2,0,-drop]) rotate([0,-45,0]) cube([PIT_SIDE,PIT_SIDE,THICK],true);
    color("pink")  translate([-PIT_SIDE/2,0,-drop]) rotate([0,45,0]) cube([PIT_SIDE,PIT_SIDE,THICK],true);
  }
}

//plate_with_holes();
//lid();

