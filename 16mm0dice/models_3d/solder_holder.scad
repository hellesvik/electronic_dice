DICE_SIDE=16.2;
THICK=1;
TAPE_SIDE=8;
FRAME_SIDE=DICE_SIDE+2;
CORNER_SIZE=8;
HOLE_SIDE=15;


module frame(){
    difference(){
        cube([FRAME_SIDE,FRAME_SIDE,THICK],true);
        cube([DICE_SIDE,DICE_SIDE,THICK],true);
    }
}
module cross(){

    cube([FRAME_SIDE,FRAME_SIDE,THICK],true);
    translate([0,0,THICK]) frame();

    translate([FRAME_SIDE/2+TAPE_SIDE/2,0,0]) cube([TAPE_SIDE,TAPE_SIDE,THICK],true);
    translate([FRAME_SIDE/2+TAPE_SIDE/2,0,THICK]) cube([TAPE_SIDE,TAPE_SIDE,THICK],true);

    translate([0,FRAME_SIDE/2+TAPE_SIDE/2,0]) cube([TAPE_SIDE,TAPE_SIDE,THICK],true);
    translate([0,FRAME_SIDE/2+TAPE_SIDE/2,THICK]) cube([TAPE_SIDE,TAPE_SIDE,THICK],true);

    translate([-FRAME_SIDE/2-TAPE_SIDE/2,0,0]) cube([TAPE_SIDE,TAPE_SIDE,THICK],true);
    translate([-FRAME_SIDE/2-TAPE_SIDE/2,0,THICK]) cube([TAPE_SIDE,TAPE_SIDE,THICK],true);

    translate([0,-FRAME_SIDE/2-TAPE_SIDE/2,0]) cube([TAPE_SIDE,TAPE_SIDE,THICK],true);
    translate([0,-FRAME_SIDE/2-TAPE_SIDE/2,THICK]) cube([TAPE_SIDE,TAPE_SIDE,THICK],true);

}

difference(){
cross();
translate([FRAME_SIDE/2,FRAME_SIDE/2,0])  cube([CORNER_SIZE,CORNER_SIZE,THICK],true);
translate([FRAME_SIDE/2,FRAME_SIDE/2,THICK])  cube([CORNER_SIZE,CORNER_SIZE,THICK],true);
cube([HOLE_SIDE,HOLE_SIDE,THICK],true);
}
