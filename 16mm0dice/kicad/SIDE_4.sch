EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 5E997AA7
P 3250 5300
AR Path="/5D8233E8/5E997AA7" Ref="#PWR?"  Part="1" 
AR Path="/5D91B0A5/5E997AA7" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 3250 5050 50  0001 C CNN
F 1 "GND" V 3255 5172 50  0000 R CNN
F 2 "" H 3250 5300 50  0001 C CNN
F 3 "" H 3250 5300 50  0001 C CNN
	1    3250 5300
	0    1    1    0   
$EndComp
Text GLabel 3250 5150 0    50   Input ~ 0
side4
Text GLabel 1700 5250 2    50   Input ~ 0
e
$Sheet
S 7350 4900 1000 700 
U 5E9A4431
F0 "7_segment_4" 50
F1 "7_segment_4.sch" 50
$EndSheet
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 5E96BCC5
P 1700 6500
AR Path="/5D8233E8/5E96BCC5" Ref="#PWR?"  Part="1" 
AR Path="/5D91B0A5/5E96BCC5" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 1700 6250 50  0001 C CNN
F 1 "GND" V 1705 6372 50  0000 R CNN
F 2 "" H 1700 6500 50  0001 C CNN
F 3 "" H 1700 6500 50  0001 C CNN
	1    1700 6500
	0    -1   -1   0   
$EndComp
Text GLabel 3250 5550 0    50   Input ~ 0
g
Text GLabel 3250 5450 0    50   Input ~ 0
b
Text GLabel 3250 5050 0    50   Input ~ 0
a
Text GLabel 3250 6250 0    50   Input ~ 0
c
Text GLabel 3250 6550 0    50   Input ~ 0
d
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A80AA6
P 3450 5400
AR Path="/5D8233E8/61A80AA6" Ref="J?"  Part="1" 
AR Path="/5D91B0A5/61A80AA6" Ref="J19"  Part="1" 
F 0 "J19" H 3478 5326 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 3478 5235 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 3450 5450 50  0001 C CNN
F 3 "" H 3450 5450 50  0001 C CNN
	1    3450 5400
	1    0    0    1   
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A80AAC
P 3450 6500
AR Path="/5D8233E8/61A80AAC" Ref="J?"  Part="1" 
AR Path="/5D91B0A5/61A80AAC" Ref="J20"  Part="1" 
F 0 "J20" H 3478 6426 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 3478 6335 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 3450 6550 50  0001 C CNN
F 3 "" H 3450 6550 50  0001 C CNN
	1    3450 6500
	1    0    0    1   
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A82422
P 1500 6400
AR Path="/5D8233E8/61A82422" Ref="J?"  Part="1" 
AR Path="/5D91B0A5/61A82422" Ref="J18"  Part="1" 
F 0 "J18" H 1600 6250 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector_bend" H 2250 6350 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5_bend" H 1500 6450 50  0001 C CNN
F 3 "" H 1500 6450 50  0001 C CNN
	1    1500 6400
	-1   0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A82428
P 1500 5300
AR Path="/5D8233E8/61A82428" Ref="J?"  Part="1" 
AR Path="/5D91B0A5/61A82428" Ref="J17"  Part="1" 
F 0 "J17" H 1600 5100 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector_bend" H 2250 5200 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5_bend" H 1500 5350 50  0001 C CNN
F 3 "" H 1500 5350 50  0001 C CNN
	1    1500 5300
	-1   0    0    -1  
$EndComp
$Comp
L dice-rescue:+BATT-Power #PWR0122
U 1 1 61ACC8F2
P 3250 6400
F 0 "#PWR0122" H 3250 6250 50  0001 C CNN
F 1 "+BATT-Power" V 3265 6528 50  0000 L CNN
F 2 "" H 3250 6400 50  0001 C CNN
F 3 "" H 3250 6400 50  0001 C CNN
	1    3250 6400
	0    -1   -1   0   
$EndComp
$Comp
L dice-rescue:+BATT-Power #PWR0123
U 1 1 61ACDBF9
P 1700 5400
F 0 "#PWR0123" H 1700 5250 50  0001 C CNN
F 1 "+BATT-Power" V 1715 5528 50  0000 L CNN
F 2 "" H 1700 5400 50  0001 C CNN
F 3 "" H 1700 5400 50  0001 C CNN
	1    1700 5400
	0    1    1    0   
$EndComp
$Comp
L dice:Conn_01x02 J33
U 1 1 61BA0E8D
P 2350 2900
F 0 "J33" V 2314 2712 50  0000 R CNN
F 1 "Conn_01x02" V 2223 2712 50  0000 R CNN
F 2 "Parts:PinHeader_1x02_P2.00mm_Vertical" H 2350 2900 50  0001 C CNN
F 3 "~" H 2350 2900 50  0001 C CNN
	1    2350 2900
	0    -1   -1   0   
$EndComp
$Comp
L dice-rescue:+BATT-Power #PWR0124
U 1 1 61BA27C1
P 2450 3100
F 0 "#PWR0124" H 2450 2950 50  0001 C CNN
F 1 "+BATT-Power" V 2465 3228 50  0000 L CNN
F 2 "" H 2450 3100 50  0001 C CNN
F 3 "" H 2450 3100 50  0001 C CNN
	1    2450 3100
	-1   0    0    1   
$EndComp
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 61BA6795
P 2350 3100
AR Path="/5D8233E8/61BA6795" Ref="#PWR?"  Part="1" 
AR Path="/5D91B0A5/61BA6795" Ref="#PWR0125"  Part="1" 
F 0 "#PWR0125" H 2350 2850 50  0001 C CNN
F 1 "GND" V 2355 2972 50  0000 R CNN
F 2 "" H 2350 3100 50  0001 C CNN
F 3 "" H 2350 3100 50  0001 C CNN
	1    2350 3100
	1    0    0    -1  
$EndComp
Text GLabel 1700 5150 2    50   Input ~ 0
UPDI_PROGRAMMING
$EndSCHEMATC
