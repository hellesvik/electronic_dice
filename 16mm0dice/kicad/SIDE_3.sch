EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dice-rescue:+BATT-Power #PWR?
U 1 1 5E9951E8
P 3850 5200
AR Path="/5D8233E8/5E9951E8" Ref="#PWR?"  Part="1" 
AR Path="/5D91A3E3/5E9951E8" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 3850 5050 50  0001 C CNN
F 1 "+BATT" V 3865 5327 50  0000 L CNN
F 2 "" H 3850 5200 50  0001 C CNN
F 3 "" H 3850 5200 50  0001 C CNN
	1    3850 5200
	0    -1   1    0   
$EndComp
Text GLabel 3650 5350 0    50   Input ~ 0
side3
Text GLabel 2250 5500 2    50   Input ~ 0
f
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 5E9926DC
P 2100 5350
AR Path="/5D8233E8/5E9926DC" Ref="#PWR?"  Part="1" 
AR Path="/5D91A3E3/5E9926DC" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 2100 5100 50  0001 C CNN
F 1 "GND" V 2105 5222 50  0000 R CNN
F 2 "" H 2100 5350 50  0001 C CNN
F 3 "" H 2100 5350 50  0001 C CNN
	1    2100 5350
	0    -1   -1   0   
$EndComp
$Sheet
S 6250 4600 1000 700 
U 5E9A4E89
F0 "7_segment_3" 50
F1 "7_segment_3.sch" 50
$EndSheet
Text GLabel 3600 5050 0    50   Input ~ 0
a
Text GLabel 3700 4250 0    50   Input ~ 0
c
Text GLabel 3700 3950 0    50   Input ~ 0
b
Text GLabel 2250 4000 2    50   Input ~ 0
g
Text GLabel 2250 5200 2    50   Input ~ 0
e
Text GLabel 2250 4500 2    50   Input ~ 0
d
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 5E96DFE0
P 3850 4100
AR Path="/5D8233E8/5E96DFE0" Ref="#PWR?"  Part="1" 
AR Path="/5D91B0A5/5E96DFE0" Ref="#PWR?"  Part="1" 
AR Path="/5D91A3E3/5E96DFE0" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 3850 3850 50  0001 C CNN
F 1 "GND" V 3855 3972 50  0000 R CNN
F 2 "" H 3850 4100 50  0001 C CNN
F 3 "" H 3850 4100 50  0001 C CNN
	1    3850 4100
	0    1    1    0   
$EndComp
Text GLabel 2250 4100 2    50   Input ~ 0
f
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A5ED18
P 4050 4000
AR Path="/5D8233E8/61A5ED18" Ref="J?"  Part="1" 
AR Path="/5D91A3E3/61A5ED18" Ref="J15"  Part="1" 
F 0 "J15" H 4078 3926 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 4078 3835 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 4050 4050 50  0001 C CNN
F 3 "" H 4050 4050 50  0001 C CNN
	1    4050 4000
	1    0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A5ED1E
P 4050 5100
AR Path="/5D8233E8/61A5ED1E" Ref="J?"  Part="1" 
AR Path="/5D91A3E3/61A5ED1E" Ref="J16"  Part="1" 
F 0 "J16" H 4078 5026 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 4078 4935 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 4050 5150 50  0001 C CNN
F 3 "" H 4050 5150 50  0001 C CNN
	1    4050 5100
	1    0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A5FDB8
P 1900 5450
AR Path="/5D8233E8/61A5FDB8" Ref="J?"  Part="1" 
AR Path="/5D91A3E3/61A5FDB8" Ref="J14"  Part="1" 
F 0 "J14" H 1792 4875 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector_bend" H 1792 4966 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5_bend" H 1900 5500 50  0001 C CNN
F 3 "" H 1900 5500 50  0001 C CNN
	1    1900 5450
	-1   0    0    1   
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A5FDBE
P 1900 4350
AR Path="/5D8233E8/61A5FDBE" Ref="J?"  Part="1" 
AR Path="/5D91A3E3/61A5FDBE" Ref="J13"  Part="1" 
F 0 "J13" H 1792 3775 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector_bend" H 1792 3866 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5_bend" H 1900 4400 50  0001 C CNN
F 3 "" H 1900 4400 50  0001 C CNN
	1    1900 4350
	-1   0    0    1   
$EndComp
$Comp
L dice-rescue:+BATT-Power #PWR?
U 1 1 61B2AE72
P 2100 4250
AR Path="/5D8233E8/61B2AE72" Ref="#PWR?"  Part="1" 
AR Path="/5D91A3E3/61B2AE72" Ref="#PWR0120"  Part="1" 
F 0 "#PWR0120" H 2100 4100 50  0001 C CNN
F 1 "+BATT" V 2115 4377 50  0000 L CNN
F 2 "" H 2100 4250 50  0001 C CNN
F 3 "" H 2100 4250 50  0001 C CNN
	1    2100 4250
	0    1    -1   0   
$EndComp
$EndSCHEMATC
