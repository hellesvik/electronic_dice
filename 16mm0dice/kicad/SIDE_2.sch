EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 2750 6200 0    50   Input ~ 0
side6
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 5E9864B8
P 2750 5550
AR Path="/5D8233E8/5E9864B8" Ref="#PWR?"  Part="1" 
AR Path="/5E9864B8" Ref="#PWR?"  Part="1" 
AR Path="/5D919717/5E9864B8" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 2750 5300 50  0001 C CNN
F 1 "GND" V 2755 5422 50  0000 R CNN
F 2 "" H 2750 5550 50  0001 C CNN
F 3 "" H 2750 5550 50  0001 C CNN
	1    2750 5550
	0    1    -1   0   
$EndComp
Text GLabel 2750 6400 0    50   Input ~ 0
twi_scl
Text GLabel 2750 6500 0    50   Input ~ 0
twi_sda
Text GLabel 2750 6300 0    50   Input ~ 0
INT1
Text GLabel 2750 6100 0    50   Input ~ 0
side2
$Sheet
S 6700 4400 1000 700 
U 5E98B949
F0 "7_segment_2" 50
F1 "7_segment_2.sch" 50
$EndSheet
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 5E96CDFC
P 1600 6650
AR Path="/5D8233E8/5E96CDFC" Ref="#PWR?"  Part="1" 
AR Path="/5D91B0A5/5E96CDFC" Ref="#PWR?"  Part="1" 
AR Path="/5D919717/5E96CDFC" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 1600 6400 50  0001 C CNN
F 1 "GND" V 1605 6522 50  0000 R CNN
F 2 "" H 1600 6650 50  0001 C CNN
F 3 "" H 1600 6650 50  0001 C CNN
	1    1600 6650
	0    -1   -1   0   
$EndComp
Text GLabel 1600 6500 2    50   Input ~ 0
c
Text GLabel 1600 6000 2    50   Input ~ 0
side6
Text GLabel 1600 6400 2    50   Input ~ 0
d
$Comp
L dice-rescue:C-Device C3
U 1 1 5E95B40C
P 2350 2950
F 0 "C3" V 2200 2950 50  0000 C CNN
F 1 "100nF" V 2500 2950 50  0000 C CNN
F 2 "Parts:C_0402_1005Metric" H 2388 2800 50  0001 C CNN
F 3 "" H 2350 2950 50  0001 C CNN
	1    2350 2950
	0    1    1    0   
$EndComp
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 5E95D452
P 2500 2950
AR Path="/5D8233E8/5E95D452" Ref="#PWR?"  Part="1" 
AR Path="/5E95D452" Ref="#PWR?"  Part="1" 
AR Path="/5D919717/5E95D452" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 2500 2700 50  0001 C CNN
F 1 "GND" H 2450 2850 50  0000 R CNN
F 2 "" H 2500 2950 50  0001 C CNN
F 3 "" H 2500 2950 50  0001 C CNN
	1    2500 2950
	-1   0    0    -1  
$EndComp
Text GLabel 900  3550 0    50   Input ~ 0
twi_scl
Text GLabel 2600 3650 2    50   Input ~ 0
INT2
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 5E96B771
P 2350 4050
AR Path="/5D8233E8/5E96B771" Ref="#PWR?"  Part="1" 
AR Path="/5E96B771" Ref="#PWR?"  Part="1" 
AR Path="/5D919717/5E96B771" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 2350 3800 50  0001 C CNN
F 1 "GND" V 2355 3922 50  0000 R CNN
F 2 "" H 2350 4050 50  0001 C CNN
F 3 "" H 2350 4050 50  0001 C CNN
	1    2350 4050
	-1   0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A74C1F
P 2950 6750
AR Path="/5D8233E8/61A74C1F" Ref="J?"  Part="1" 
AR Path="/5D919717/61A74C1F" Ref="J12"  Part="1" 
F 0 "J12" H 2978 6676 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 2978 6585 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 2950 6800 50  0001 C CNN
F 3 "" H 2950 6800 50  0001 C CNN
	1    2950 6750
	1    0    0    1   
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector J?
U 1 1 61A765DA
P 1400 6000
AR Path="/5D8233E8/61A765DA" Ref="J?"  Part="1" 
AR Path="/5D919717/61A765DA" Ref="J8"  Part="1" 
F 0 "J8" H 1500 5700 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector" H 2050 5800 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5" H 1400 6000 50  0001 C CNN
F 3 "" H 1400 6000 50  0001 C CNN
	1    1400 6000
	-1   0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A765E0
P 1400 6550
AR Path="/5D8233E8/61A765E0" Ref="J?"  Part="1" 
AR Path="/5D919717/61A765E0" Ref="J9"  Part="1" 
F 0 "J9" H 1500 6450 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector_bend" H 2150 6550 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5_bend" H 1400 6600 50  0001 C CNN
F 3 "" H 1400 6600 50  0001 C CNN
	1    1400 6550
	-1   0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A765E6
P 1400 5450
AR Path="/5D8233E8/61A765E6" Ref="J?"  Part="1" 
AR Path="/5D919717/61A765E6" Ref="J7"  Part="1" 
F 0 "J7" H 1500 5300 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector_bend" H 2150 5400 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5_bend" H 1400 5500 50  0001 C CNN
F 3 "" H 1400 5500 50  0001 C CNN
	1    1400 5450
	-1   0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A74C19
P 2950 5650
AR Path="/5D8233E8/61A74C19" Ref="J?"  Part="1" 
AR Path="/5D919717/61A74C19" Ref="J10"  Part="1" 
F 0 "J10" H 2978 5576 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 2978 5485 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 2950 5700 50  0001 C CNN
F 3 "" H 2950 5700 50  0001 C CNN
	1    2950 5650
	1    0    0    1   
$EndComp
$Comp
L dice:MMA8653FCR1 U2
U 1 1 61BC0BCA
P 2200 3650
F 0 "U2" H 2550 4050 50  0000 L CNN
F 1 "MMA8653FCR1" H 2550 3950 50  0000 L CNN
F 2 "Parts:DFN-10_2x2mm_P0.4mm" H 2250 3200 50  0001 L CNN
F 3 "http://cache.freescale.com/files/sensors/doc/data_sheet/MMA8653FC.pdf" H 2000 3600 50  0001 C CNN
	1    2200 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 4050 2200 4050
Connection ~ 2100 4050
Wire Wire Line
	2100 4050 2000 4050
Connection ~ 2200 4050
Wire Wire Line
	2200 4050 2100 4050
$Comp
L dice:+3.3V-power #PWR0113
U 1 1 61BC54E8
P 2200 2850
F 0 "#PWR0113" H 2200 2700 50  0001 C CNN
F 1 "+3.3V-power" H 2200 3150 50  0000 C CNN
F 2 "" H 2200 2850 50  0001 C CNN
F 3 "" H 2200 2850 50  0001 C CNN
	1    2200 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3250 2200 3150
Connection ~ 2200 2950
Wire Wire Line
	2200 2950 2200 2850
Wire Wire Line
	2300 3250 2200 3250
Connection ~ 2200 3250
Text GLabel 2600 3750 2    50   Input ~ 0
INT1
Text GLabel 2750 6000 0    50   Input ~ 0
INT2
Text GLabel 2750 5800 0    50   Input ~ 0
b
Text GLabel 2750 5700 0    50   Input ~ 0
a
$Comp
L dice-rescue:R-Device R9
U 1 1 61BEB757
P 1600 3300
F 0 "R9" H 1450 3350 50  0000 L CNN
F 1 "1K" H 1450 3250 50  0000 L CNN
F 2 "Parts:R_0402_1005Metric" V 1530 3300 50  0001 C CNN
F 3 "" H 1600 3300 50  0001 C CNN
	1    1600 3300
	1    0    0    -1  
$EndComp
$Comp
L dice-rescue:R-Device R8
U 1 1 61BEBAEA
P 950 3350
F 0 "R8" H 800 3400 50  0000 L CNN
F 1 "1K" H 800 3300 50  0000 L CNN
F 2 "Parts:R_0402_1005Metric" V 880 3350 50  0001 C CNN
F 3 "" H 950 3350 50  0001 C CNN
	1    950  3350
	1    0    0    -1  
$EndComp
Text GLabel 1400 3450 0    50   Input ~ 0
twi_sda
Wire Wire Line
	950  3500 950  3550
Wire Wire Line
	950  3550 900  3550
Wire Wire Line
	950  3550 1800 3550
Connection ~ 950  3550
Connection ~ 1600 3450
Wire Wire Line
	1600 3450 1800 3450
Wire Wire Line
	1400 3450 1600 3450
Wire Wire Line
	950  3200 950  3150
Wire Wire Line
	950  3150 1600 3150
Wire Wire Line
	1600 3150 2200 3150
Connection ~ 1600 3150
Connection ~ 2200 3150
Wire Wire Line
	2200 3150 2200 2950
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 61BF1DA2
P 1650 3950
AR Path="/5D8233E8/61BF1DA2" Ref="#PWR?"  Part="1" 
AR Path="/61BF1DA2" Ref="#PWR?"  Part="1" 
AR Path="/5D919717/61BF1DA2" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 1650 3700 50  0001 C CNN
F 1 "GND" H 1750 3800 50  0000 R CNN
F 2 "" H 1650 3950 50  0001 C CNN
F 3 "" H 1650 3950 50  0001 C CNN
	1    1650 3950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1800 3650 1650 3650
$Comp
L dice-rescue:C-Device C2
U 1 1 61BF1D9C
P 1650 3800
F 0 "C2" H 1500 3800 50  0000 C CNN
F 1 "100nF" V 1800 3800 50  0000 C CNN
F 2 "Parts:C_0402_1005Metric" H 1688 3650 50  0001 C CNN
F 3 "" H 1650 3800 50  0001 C CNN
	1    1650 3800
	-1   0    0    1   
$EndComp
$Comp
L dice:+3.3V-power #PWR0112
U 1 1 61BFD6BE
P 2750 6650
F 0 "#PWR0112" H 2750 6500 50  0001 C CNN
F 1 "+3.3V-power" H 2750 6950 50  0000 C CNN
F 2 "" H 2750 6650 50  0001 C CNN
F 3 "" H 2750 6650 50  0001 C CNN
	1    2750 6650
	0    -1   -1   0   
$EndComp
$Comp
L dice:+3.3V-power #PWR0116
U 1 1 61BFF0DC
P 1600 5550
F 0 "#PWR0116" H 1600 5400 50  0001 C CNN
F 1 "+3.3V-power" H 1600 5850 50  0000 C CNN
F 2 "" H 1600 5550 50  0001 C CNN
F 3 "" H 1600 5550 50  0001 C CNN
	1    1600 5550
	0    1    1    0   
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector J?
U 1 1 61A74C13
P 2950 6200
AR Path="/5D8233E8/61A74C13" Ref="J?"  Part="1" 
AR Path="/5D919717/61A74C13" Ref="J11"  Part="1" 
F 0 "J11" H 2978 6126 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector" H 2978 6035 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5" H 2950 6200 50  0001 C CNN
F 3 "" H 2950 6200 50  0001 C CNN
	1    2950 6200
	1    0    0    1   
$EndComp
Text GLabel 1600 6300 2    50   Input ~ 0
g
Text GLabel 1600 6200 2    50   Input ~ 0
e
Text GLabel 1600 6100 2    50   Input ~ 0
f
$EndSCHEMATC
