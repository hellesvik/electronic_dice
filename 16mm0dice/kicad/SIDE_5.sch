EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1300 4800 2    50   Input ~ 0
side5
$Comp
L dice-rescue:+BATT-Power #PWR?
U 1 1 5E987EB7
P 1300 4550
AR Path="/5D8233E8/5E987EB7" Ref="#PWR?"  Part="1" 
AR Path="/5E987EB7" Ref="#PWR?"  Part="1" 
AR Path="/5D8DA265/5E987EB7" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 1300 4400 50  0001 C CNN
F 1 "+BATT" V 1315 4677 50  0000 L CNN
F 2 "" H 1300 4550 50  0001 C CNN
F 3 "" H 1300 4550 50  0001 C CNN
	1    1300 4550
	0    1    -1   0   
$EndComp
Text GLabel 1300 5200 2    50   Input ~ 0
d
Text GLabel 1300 5100 2    50   Input ~ 0
g
Text GLabel 1300 5000 2    50   Input ~ 0
b
Text GLabel 1300 5300 2    50   Input ~ 0
c
$Sheet
S 7500 4700 1000 700 
U 5E98CEBB
F0 "7_segment_5" 50
F1 "7_segment_5.sch" 50
$EndSheet
Text GLabel 1300 4900 2    50   Input ~ 0
a
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 5E96F3A0
P 2450 4500
AR Path="/5D8233E8/5E96F3A0" Ref="#PWR?"  Part="1" 
AR Path="/5D91B0A5/5E96F3A0" Ref="#PWR?"  Part="1" 
AR Path="/5D8DA265/5E96F3A0" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 2450 4250 50  0001 C CNN
F 1 "GND" V 2455 4372 50  0000 R CNN
F 2 "" H 2450 4500 50  0001 C CNN
F 3 "" H 2450 4500 50  0001 C CNN
	1    2450 4500
	0    1    1    0   
$EndComp
Text GLabel 2450 5850 0    50   Input ~ 0
c
Text GLabel 2450 4650 0    50   Input ~ 0
f
Text GLabel 2450 4750 0    50   Input ~ 0
a
Text GLabel 2450 4850 0    50   Input ~ 0
b
Text GLabel 2450 4950 0    50   Input ~ 0
g
Text GLabel 2450 5050 0    50   Input ~ 0
d
Text GLabel 2450 5150 0    50   Input ~ 0
c
Text GLabel 2450 5250 0    50   Input ~ 0
e
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A79D66
P 1100 5550
AR Path="/5D8233E8/61A79D66" Ref="J?"  Part="1" 
AR Path="/5D8DA265/61A79D66" Ref="J23"  Part="1" 
F 0 "J23" H 1200 5350 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector_bend" H 1850 5450 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5_bend" H 1100 5600 50  0001 C CNN
F 3 "" H 1100 5600 50  0001 C CNN
	1    1100 5550
	-1   0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A79D6C
P 1100 4450
AR Path="/5D8233E8/61A79D6C" Ref="J?"  Part="1" 
AR Path="/5D8DA265/61A79D6C" Ref="J21"  Part="1" 
F 0 "J21" H 1200 4300 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector_bend" H 1850 4400 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5_bend" H 1100 4500 50  0001 C CNN
F 3 "" H 1100 4500 50  0001 C CNN
	1    1100 4450
	-1   0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector J?
U 1 1 61A7B743
P 2650 5150
AR Path="/5D8233E8/61A7B743" Ref="J?"  Part="1" 
AR Path="/5D8DA265/61A7B743" Ref="J25"  Part="1" 
F 0 "J25" H 2678 5076 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector" H 2678 4985 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5" H 2650 5150 50  0001 C CNN
F 3 "" H 2650 5150 50  0001 C CNN
	1    2650 5150
	1    0    0    1   
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A7B749
P 2650 4600
AR Path="/5D8233E8/61A7B749" Ref="J?"  Part="1" 
AR Path="/5D8DA265/61A7B749" Ref="J24"  Part="1" 
F 0 "J24" H 2678 4526 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 2678 4435 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 2650 4650 50  0001 C CNN
F 3 "" H 2650 4650 50  0001 C CNN
	1    2650 4600
	1    0    0    1   
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A7B74F
P 2650 5700
AR Path="/5D8233E8/61A7B74F" Ref="J?"  Part="1" 
AR Path="/5D8DA265/61A7B74F" Ref="J26"  Part="1" 
F 0 "J26" H 2678 5626 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 2678 5535 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 2650 5750 50  0001 C CNN
F 3 "" H 2650 5750 50  0001 C CNN
	1    2650 5700
	1    0    0    1   
$EndComp
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 5E987EBD
P 1300 5650
AR Path="/5D8233E8/5E987EBD" Ref="#PWR?"  Part="1" 
AR Path="/5E987EBD" Ref="#PWR?"  Part="1" 
AR Path="/5D8DA265/5E987EBD" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 1300 5400 50  0001 C CNN
F 1 "GND" V 1305 5522 50  0000 R CNN
F 2 "" H 1300 5650 50  0001 C CNN
F 3 "" H 1300 5650 50  0001 C CNN
	1    1300 5650
	0    -1   1    0   
$EndComp
Text GLabel 1300 4300 2    50   Input ~ 0
f
Text GLabel 1300 5400 2    50   Input ~ 0
e
Text GLabel 1300 5500 2    50   Input ~ 0
f
Text GLabel 1300 5800 2    50   Input ~ 0
b
Text GLabel 1300 5900 2    50   Input ~ 0
g
$Comp
L dice-rescue:+BATT-Power #PWR?
U 1 1 61C29488
P 2450 5600
AR Path="/5D8233E8/61C29488" Ref="#PWR?"  Part="1" 
AR Path="/61C29488" Ref="#PWR?"  Part="1" 
AR Path="/5D8DA265/61C29488" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 2450 5450 50  0001 C CNN
F 1 "+BATT" V 2465 5727 50  0000 L CNN
F 2 "" H 2450 5600 50  0001 C CNN
F 3 "" H 2450 5600 50  0001 C CNN
	1    2450 5600
	0    -1   1    0   
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector J?
U 1 1 61A79D60
P 1100 5000
AR Path="/5D8233E8/61A79D60" Ref="J?"  Part="1" 
AR Path="/5D8DA265/61A79D60" Ref="J22"  Part="1" 
F 0 "J22" H 1200 4700 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector" H 1750 4800 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5" H 1100 5000 50  0001 C CNN
F 3 "" H 1100 5000 50  0001 C CNN
	1    1100 5000
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
