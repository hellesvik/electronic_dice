EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 7800 4800 1000 700 
U 5E9AD942
F0 "7_segment_6" 50
F1 "7_segment_6.sch" 50
$EndSheet
Text GLabel 3100 6050 0    50   Input ~ 0
b
Text GLabel 2100 6050 2    50   Input ~ 0
g
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 5E96C5A0
P 1650 5050
AR Path="/5D8233E8/5E96C5A0" Ref="#PWR?"  Part="1" 
AR Path="/5D91B0A5/5E96C5A0" Ref="#PWR?"  Part="1" 
AR Path="/5D91BD69/5E96C5A0" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 1650 4800 50  0001 C CNN
F 1 "GND" V 1655 4922 50  0000 R CNN
F 2 "" H 1650 5050 50  0001 C CNN
F 3 "" H 1650 5050 50  0001 C CNN
	1    1650 5050
	0    -1   -1   0   
$EndComp
$Comp
L dice-rescue:GND-Power #PWR?
U 1 1 5E96D7F7
P 3550 6100
AR Path="/5D8233E8/5E96D7F7" Ref="#PWR?"  Part="1" 
AR Path="/5D91B0A5/5E96D7F7" Ref="#PWR?"  Part="1" 
AR Path="/5D91BD69/5E96D7F7" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 3550 5850 50  0001 C CNN
F 1 "GND" V 3555 5972 50  0000 R CNN
F 2 "" H 3550 6100 50  0001 C CNN
F 3 "" H 3550 6100 50  0001 C CNN
	1    3550 6100
	0    1    1    0   
$EndComp
Text GLabel 3100 5150 0    50   Input ~ 0
b
Text GLabel 1650 5500 2    50   Input ~ 0
e
Text GLabel 3100 5350 0    50   Input ~ 0
c
Text GLabel 3100 5450 0    50   Input ~ 0
d
Text GLabel 3100 5550 0    50   Input ~ 0
g
Text GLabel 3100 5650 0    50   Input ~ 0
b
Text GLabel 3100 5750 0    50   Input ~ 0
a
Text GLabel 3100 5850 0    50   Input ~ 0
f
Text GLabel 3100 5250 0    50   Input ~ 0
e
Text GLabel 1650 5200 2    50   Input ~ 0
c
Text GLabel 1650 5300 2    50   Input ~ 0
d
Text GLabel 1650 5400 2    50   Input ~ 0
g
Text GLabel 2100 5650 2    50   Input ~ 0
b
Text GLabel 2100 5750 2    50   Input ~ 0
a
Text GLabel 1650 5600 2    50   Input ~ 0
f
Text GLabel 1650 5700 2    50   Input ~ 0
side6
$Comp
L dice:Conn_01x05_Female-Connector J?
U 1 1 61A7CFD2
P 3750 5450
AR Path="/5D8233E8/61A7CFD2" Ref="J?"  Part="1" 
AR Path="/5D91BD69/61A7CFD2" Ref="J31"  Part="1" 
F 0 "J31" H 3778 5376 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector" H 3778 5285 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5" H 3750 5450 50  0001 C CNN
F 3 "" H 3750 5450 50  0001 C CNN
	1    3750 5450
	1    0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A7CFD8
P 3750 4900
AR Path="/5D8233E8/61A7CFD8" Ref="J?"  Part="1" 
AR Path="/5D91BD69/61A7CFD8" Ref="J30"  Part="1" 
F 0 "J30" H 3778 4826 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 3778 4735 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 3750 4950 50  0001 C CNN
F 3 "" H 3750 4950 50  0001 C CNN
	1    3750 4900
	1    0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A7CFDE
P 3750 6000
AR Path="/5D8233E8/61A7CFDE" Ref="J?"  Part="1" 
AR Path="/5D91BD69/61A7CFDE" Ref="J32"  Part="1" 
F 0 "J32" H 3778 5926 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 3778 5835 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 3750 6050 50  0001 C CNN
F 3 "" H 3750 6050 50  0001 C CNN
	1    3750 6000
	1    0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector J?
U 1 1 61A7E943
P 1450 5500
AR Path="/5D8233E8/61A7E943" Ref="J?"  Part="1" 
AR Path="/5D91BD69/61A7E943" Ref="J28"  Part="1" 
F 0 "J28" H 1600 5450 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector" H 2100 5550 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5" H 1450 5500 50  0001 C CNN
F 3 "" H 1450 5500 50  0001 C CNN
	1    1450 5500
	-1   0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A7E949
P 1450 6050
AR Path="/5D8233E8/61A7E949" Ref="J?"  Part="1" 
AR Path="/5D91BD69/61A7E949" Ref="J29"  Part="1" 
F 0 "J29" H 1600 5950 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector_bend" H 2250 6050 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5_bend" H 1450 6100 50  0001 C CNN
F 3 "" H 1450 6100 50  0001 C CNN
	1    1450 6050
	-1   0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J?
U 1 1 61A7E94F
P 1450 4950
AR Path="/5D8233E8/61A7E94F" Ref="J?"  Part="1" 
AR Path="/5D91BD69/61A7E94F" Ref="J27"  Part="1" 
F 0 "J27" H 1550 4850 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector_bend" H 2200 4950 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5_bend" H 1450 5000 50  0001 C CNN
F 3 "" H 1450 5000 50  0001 C CNN
	1    1450 4950
	-1   0    0    -1  
$EndComp
$Comp
L dice-rescue:+BATT-Power #PWR?
U 1 1 61B55DBB
P 1650 6150
AR Path="/5D8233E8/61B55DBB" Ref="#PWR?"  Part="1" 
AR Path="/5D91BD69/61B55DBB" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 1650 6000 50  0001 C CNN
F 1 "+BATT" V 1665 6277 50  0000 L CNN
F 2 "" H 1650 6150 50  0001 C CNN
F 3 "" H 1650 6150 50  0001 C CNN
	1    1650 6150
	0    1    1    0   
$EndComp
$EndSCHEMATC
