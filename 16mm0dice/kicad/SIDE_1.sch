EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 4600 1350 2    50   Input ~ 0
UPDI_PROGRAMMING
Text GLabel 5200 1950 2    50   Input ~ 0
b
Text GLabel 5200 2050 2    50   Input ~ 0
g
Text GLabel 5200 2650 2    50   Input ~ 0
c
Text GLabel 5200 2750 2    50   Input ~ 0
d
$Comp
L dice-rescue:GND-Power #PWR02
U 1 1 5E689FDE
P 4000 2950
F 0 "#PWR02" H 4000 2700 50  0001 C CNN
F 1 "GND" H 4005 2777 50  0000 C CNN
F 2 "" H 4000 2950 50  0001 C CNN
F 3 "" H 4000 2950 50  0001 C CNN
	1    4000 2950
	1    0    0    -1  
$EndComp
$Comp
L dice-rescue:+BATT-Power #PWR01
U 1 1 5E68AEEB
P 4000 850
F 0 "#PWR01" H 4000 700 50  0001 C CNN
F 1 "+BATT" H 4015 1023 50  0000 C CNN
F 2 "" H 4000 850 50  0001 C CNN
F 3 "" H 4000 850 50  0001 C CNN
	1    4000 850 
	1    0    0    -1  
$EndComp
$Comp
L dice-rescue:C-Device C1
U 1 1 5E68BB05
P 3850 1000
F 0 "C1" V 3598 1000 50  0000 C CNN
F 1 "C" V 3689 1000 50  0000 C CNN
F 2 "Parts:C_0402_1005Metric" H 3888 850 50  0001 C CNN
F 3 "" H 3850 1000 50  0001 C CNN
	1    3850 1000
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 1150 4000 1000
Wire Wire Line
	4000 850  4000 1000
Connection ~ 4000 1000
Text GLabel 4600 1550 2    50   Input ~ 0
side3
Text GLabel 4600 1750 2    50   Input ~ 0
side5
Text GLabel 3400 2450 0    50   Input ~ 0
side6
Text GLabel 4800 4300 0    50   Input ~ 0
c
Text GLabel 4800 4000 0    50   Input ~ 0
b
Text GLabel 4800 4100 0    50   Input ~ 0
g
Text GLabel 4800 4200 0    50   Input ~ 0
d
$Comp
L dice-rescue:GND-Power #PWR0105
U 1 1 5E6B4925
P 3700 1000
F 0 "#PWR0105" H 3700 750 50  0001 C CNN
F 1 "GND" H 3705 827 50  0000 C CNN
F 2 "" H 3700 1000 50  0001 C CNN
F 3 "" H 3700 1000 50  0001 C CNN
	1    3700 1000
	1    0    0    -1  
$EndComp
Text GLabel 4800 3400 0    50   Input ~ 0
side3
Text GLabel 3100 4300 2    50   Input ~ 0
side6
$Comp
L dice-rescue:GND-Power #PWR0102
U 1 1 5E6AD178
P 3100 3650
F 0 "#PWR0102" H 3100 3400 50  0001 C CNN
F 1 "GND" V 3105 3522 50  0000 R CNN
F 2 "" H 3100 3650 50  0001 C CNN
F 3 "" H 3100 3650 50  0001 C CNN
	1    3100 3650
	0    -1   -1   0   
$EndComp
Text GLabel 4600 2250 2    50   Input ~ 0
twi_scl
Text GLabel 4600 2350 2    50   Input ~ 0
twi_sda
Text GLabel 4600 1650 2    50   Input ~ 0
side1
$Comp
L dice-rescue:R-Device R4
U 1 1 5E6690C1
P 5050 1950
F 0 "R4" V 5050 2000 50  0000 C CNN
F 1 "R" V 5050 1900 50  0000 C CNN
F 2 "Parts:R_0402_1005Metric" V 4980 1950 50  0001 C CNN
F 3 "" H 5050 1950 50  0001 C CNN
	1    5050 1950
	0    1    1    0   
$EndComp
$Comp
L dice-rescue:R-Device R5
U 1 1 5E6694F7
P 5050 2050
F 0 "R5" V 5050 2100 50  0000 C CNN
F 1 "R" V 5050 2000 50  0000 C CNN
F 2 "Parts:R_0402_1005Metric" V 4980 2050 50  0001 C CNN
F 3 "" H 5050 2050 50  0001 C CNN
	1    5050 2050
	0    1    1    0   
$EndComp
$Comp
L dice-rescue:R-Device R6
U 1 1 5E68955F
P 5050 2650
F 0 "R6" V 5050 2700 50  0000 C CNN
F 1 "R" V 5050 2600 50  0000 C CNN
F 2 "Parts:R_0402_1005Metric" V 4980 2650 50  0001 C CNN
F 3 "" H 5050 2650 50  0001 C CNN
	1    5050 2650
	0    1    1    0   
$EndComp
$Comp
L dice-rescue:R-Device R7
U 1 1 5E68993F
P 5050 2750
F 0 "R7" V 5050 2800 50  0000 C CNN
F 1 "R" V 5050 2700 50  0000 C CNN
F 2 "Parts:R_0402_1005Metric" V 4980 2750 50  0001 C CNN
F 3 "" H 5050 2750 50  0001 C CNN
	1    5050 2750
	0    1    1    0   
$EndComp
Text GLabel 3100 4500 2    50   Input ~ 0
twi_scl
Text GLabel 3100 4600 2    50   Input ~ 0
twi_sda
Text GLabel 3100 4400 2    50   Input ~ 0
INT1
Text GLabel 3100 5000 2    50   Input ~ 0
UPDI_PROGRAMMING
Text GLabel 3100 3400 2    50   Input ~ 0
f
Text GLabel 3100 4200 2    50   Input ~ 0
side2
$Comp
L dice-rescue:R-Device R1
U 1 1 5E68D517
P 5050 2550
F 0 "R1" V 5050 2600 50  0000 C CNN
F 1 "R" V 5050 2500 50  0000 C CNN
F 2 "Parts:R_0402_1005Metric" V 4980 2550 50  0001 C CNN
F 3 "" H 5050 2550 50  0001 C CNN
	1    5050 2550
	0    -1   -1   0   
$EndComp
Text GLabel 5200 2550 2    50   Input ~ 0
e
Wire Wire Line
	4600 2750 4900 2750
Wire Wire Line
	4900 2650 4600 2650
Wire Wire Line
	4600 2550 4900 2550
Wire Wire Line
	4600 2050 4900 2050
Wire Wire Line
	4900 1950 4600 1950
$Comp
L dice-rescue:ATtiny1616-M-MCU_Michrochip_ATtiny U1
U 1 1 5E650C55
P 4000 2050
F 0 "U1" H 4200 3100 50  0000 C CNN
F 1 "ATtiny1616-M" H 4400 3000 50  0000 C CNN
F 2 "Parts:QFN-20-1EP_4x4mm_P0.5mm_EP2.5x2.5mm" H 4000 2050 50  0001 C CIN
F 3 "" H 4000 2050 50  0001 C CNN
	1    4000 2050
	1    0    0    -1  
$EndComp
$Sheet
S 9100 2450 1000 700 
U 5E54907D
F0 "7_segment_1" 50
F1 "7_segment_1.sch" 50
$EndSheet
Text GLabel 3100 4900 2    50   Input ~ 0
e
$Comp
L dice-rescue:R-Device R3
U 1 1 5E668D61
P 5050 1850
F 0 "R3" V 5050 1900 50  0000 C CNN
F 1 "R" V 5050 1800 50  0000 C CNN
F 2 "Parts:R_0402_1005Metric" V 4980 1850 50  0001 C CNN
F 3 "" H 5050 1850 50  0001 C CNN
	1    5050 1850
	0    1    1    0   
$EndComp
Text GLabel 5200 1850 2    50   Input ~ 0
a
Wire Wire Line
	4900 1850 4600 1850
$Comp
L dice:Conn_01x05_Female-Connector J5
U 1 1 61A49566
P 5000 4200
F 0 "J5" H 5028 4126 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector" H 5028 4035 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5" H 5000 4200 50  0001 C CNN
F 3 "" H 5000 4200 50  0001 C CNN
	1    5000 4200
	1    0    0    1   
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J6
U 1 1 61A4B9BF
P 5000 4750
F 0 "J6" H 5028 4676 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 5028 4585 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 5000 4800 50  0001 C CNN
F 3 "" H 5000 4800 50  0001 C CNN
	1    5000 4750
	1    0    0    1   
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector J2
U 1 1 61A4F415
P 2900 4100
F 0 "J2" H 2928 4026 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector" H 2928 3935 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5" H 2900 4100 50  0001 C CNN
F 3 "" H 2900 4100 50  0001 C CNN
	1    2900 4100
	-1   0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J3
U 1 1 61A4F41B
P 2900 4650
F 0 "J3" H 3000 4650 50  0000 C CNN
F 1 "Conn_01x05_Female-Connector_bend" H 3650 4550 50  0000 C CNN
F 2 "Parts:castellated_hole_1x5_bend" H 2900 4700 50  0001 C CNN
F 3 "" H 2900 4700 50  0001 C CNN
	1    2900 4650
	-1   0    0    -1  
$EndComp
$Comp
L dice:Conn_01x05_Female-Connector_bend J1
U 1 1 61A4F421
P 2900 3550
F 0 "J1" H 2928 3476 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 2928 3385 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 2900 3600 50  0001 C CNN
F 3 "" H 2900 3600 50  0001 C CNN
	1    2900 3550
	-1   0    0    -1  
$EndComp
Text GLabel 4800 4800 0    50   Input ~ 0
side4
$Comp
L dice-rescue:GND-Power #PWR0104
U 1 1 61B13215
P 4800 4650
F 0 "#PWR0104" H 4800 4400 50  0001 C CNN
F 1 "GND" V 4805 4522 50  0000 R CNN
F 2 "" H 4800 4650 50  0001 C CNN
F 3 "" H 4800 4650 50  0001 C CNN
	1    4800 4650
	0    1    1    0   
$EndComp
$Comp
L dice-rescue:+BATT-Power #PWR0118
U 1 1 61B144D7
P 4800 3550
F 0 "#PWR0118" H 4800 3400 50  0001 C CNN
F 1 "+BATT" V 4815 3677 50  0000 L CNN
F 2 "" H 4800 3550 50  0001 C CNN
F 3 "" H 4800 3550 50  0001 C CNN
	1    4800 3550
	0    -1   -1   0   
$EndComp
Text GLabel 4800 3900 0    50   Input ~ 0
a
Text GLabel 4800 4400 0    50   Input ~ 0
e
Text GLabel 4800 4500 0    50   Input ~ 0
f
$Comp
L dice:Conn_01x05_Female-Connector_bend J4
U 1 1 61A4A896
P 5000 3650
F 0 "J4" H 5028 3576 50  0000 L CNN
F 1 "Conn_01x05_Female-Connector_bend" H 5028 3485 50  0000 L CNN
F 2 "Parts:castellated_hole_1x5_bend" H 5000 3700 50  0001 C CNN
F 3 "" H 5000 3700 50  0001 C CNN
	1    5000 3650
	1    0    0    1   
$EndComp
Text GLabel 4800 3800 0    50   Input ~ 0
side5
Text GLabel 3400 2350 0    50   Input ~ 0
INT1
Text GLabel 3400 2550 0    50   Input ~ 0
side2
Text GLabel 3400 2250 0    50   Input ~ 0
side4
Wire Wire Line
	4900 2450 4600 2450
Text GLabel 5200 2450 2    50   Input ~ 0
f
$Comp
L dice-rescue:R-Device R2
U 1 1 5E68CDEF
P 5050 2450
F 0 "R2" V 5050 2500 50  0000 C CNN
F 1 "R" V 5050 2400 50  0000 C CNN
F 2 "Parts:R_0402_1005Metric" V 4980 2450 50  0001 C CNN
F 3 "" H 5050 2450 50  0001 C CNN
	1    5050 2450
	0    -1   -1   0   
$EndComp
Text GLabel 4600 1450 2    50   Input ~ 0
INT2
Text GLabel 3100 4100 2    50   Input ~ 0
INT2
$Comp
L dice-rescue:+BATT-Power #PWR0111
U 1 1 5E92D2D6
P 3100 4750
F 0 "#PWR0111" H 3100 4600 50  0001 C CNN
F 1 "+BATT" V 3115 4877 50  0000 L CNN
F 2 "" H 3100 4750 50  0001 C CNN
F 3 "" H 3100 4750 50  0001 C CNN
	1    3100 4750
	0    1    -1   0   
$EndComp
Text GLabel 3100 3900 2    50   Input ~ 0
b
Text GLabel 3100 3800 2    50   Input ~ 0
a
Text GLabel 3100 3500 2    50   Input ~ 0
e
$EndSCHEMATC
