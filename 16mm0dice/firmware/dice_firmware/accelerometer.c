#include <avr/io.h>
#ifndef F_CPU
#define F_CPU 20000000
#endif
#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>
#include "segments_control.h"
#include "accelerometer.h"

#define TWI0_BAUD(F_SCL)      ((((float)F_CPU / (float)F_SCL)) - 10 )

#define I2C_SCL_FREQ                                    100000

// I2C_base_ADDRESS                                     0b1000000  //address with 7 bits
#define I2C_BASE_ADDRESS 0x1d

#define I2C_DIRECTION_BIT_WRITE                         0
#define I2C_DIRECTION_BIT_READ                          1



static void I2C_0_init(void)
{
	PORTB.DIRSET = (1<<0);
	PORTB.DIRSET = (1<<1);
	PORTB.OUTCLR = (1<<0);
	TWI0.MBAUD = (uint8_t)TWI0_BAUD(I2C_SCL_FREQ);	// set MBAUD register for I2C_SCL_Freq Hz
	TWI0.MCTRLA = TWI_ENABLE_bm;
	TWI0.CTRLA |= (1<<3);
	TWI0.CTRLA |= (1<<2);
	TWI0.MSTATUS = TWI_BUSSTATE_IDLE_gc;
}

static void I2C_0_uninit(void)
{
	TWI0.MCTRLA &= ~TWI_ENABLE_bm;
}

static uint8_t I2C_0_start(uint8_t baseAddres, uint8_t directionBit)
{
	TWI0.MADDR = (baseAddres<<1) + directionBit;
	while (!(TWI0.MSTATUS & (TWI_WIF_bm | TWI_RIF_bm)));    //wait for write or read interrupt flag
	if ((TWI0.MSTATUS & TWI_ARBLOST_bm)) return 0 ;         //return 0 if bus error or arbitration lost
	return !(TWI0.MSTATUS & TWI_RXACK_bm);                  //return 1 if slave gave an ack
}


static uint8_t I2C_0_writingPacket(uint8_t data)
{
	while (!(TWI0.MSTATUS & TWI_WIF_bm));               //wait for write interrupt flag
	TWI0.MDATA = data;
	TWI0.MCTRLB = TWI_MCMD_RECVTRANS_gc;
	return (!(TWI0.MSTATUS & TWI_RXACK_bm));        //returns 1 if slave gave an ack
}

static uint8_t I2C_0_receivingPacket(uint8_t acknack)      // 0 -> ack, else nack
{
	while(!(TWI0.MSTATUS & TWI_RIF_bm));              //wait for read interrupt flag
	uint8_t data = TWI0.MDATA;
	if (acknack == 0) {TWI0.MCTRLB = (TWI_ACKACT_ACK_gc  | TWI_MCMD_RECVTRANS_gc); }
	else                {TWI0.MCTRLB = (TWI_ACKACT_NACK_gc | TWI_MCMD_RECVTRANS_gc); }
    return data;
}

uint8_t acc_read(uint8_t reg){
	I2C_0_start(I2C_BASE_ADDRESS, I2C_DIRECTION_BIT_WRITE);
	I2C_0_writingPacket(reg);
	I2C_0_start(I2C_BASE_ADDRESS, I2C_DIRECTION_BIT_READ);  
	return I2C_0_receivingPacket(1);
}

int8_t acc_write_byte(uint8_t reg, uint8_t data){
	uint8_t err = 0;
	I2C_0_start(I2C_BASE_ADDRESS, I2C_DIRECTION_BIT_WRITE);
	I2C_0_writingPacket(reg);
	I2C_0_writingPacket(data);
	_delay_ms(5);
	err = acc_read(reg);
	if ( err != data){
		return -1;
	}
	return 0; 
}


void acc_roll(){

  uint8_t data = 0;
  I2C_0_init();
  data = acc_read(ACC_OUT_Y_MSB);
  dice_roll(data);
  _delay_ms(70);
  dice_clear();
  acc_read(ACC_FF_MT_SRC); // Clear flag
                           //
  I2C_0_uninit();

}


int8_t acc_enable(){

	volatile uint8_t err = 0;
	I2C_0_init();
	_delay_ms(20);
	volatile uint8_t data = 0;
	data = acc_read(ACC_WHO_AM_I);
	if(data != EXPECTED_ADDR){
		return 8;
	}

  err = acc_write_byte(ACC_CTRL_REG1, 0);

  err = acc_write_byte(ACC_XYZ_DATA_CFG, 0);
  if(err){
    return 8;
  }

  err = acc_write_byte(ACC_FF_MT_CFG, (1<<3) | (1<<4) | (1<<5) | (1<<6) | (1<<7));
  if(err){
    return 8;
  }

  err = acc_write_byte(ACC_FF_MT_THS, 18);
  if(err){
    return 8;
  }

  err = acc_write_byte(ACC_FF_MT_COUNT, 3);
  if(err){
    return 8;
  }

  err = acc_write_byte(ACC_ASLP_COUNT,5);
  if(err){
    return 8;
  }

  err = acc_write_byte(ACC_CTRL_REG2, (1<<0) | (1<<1) | (1<<2) | (1<<3) | (1<<4));
  if(err){
    return 8;
  }

  err = acc_write_byte(ACC_CTRL_REG3, (1<<3));
  if(err){
    return 8;
  }

  err = acc_write_byte(ACC_CTRL_REG4, (1<<2));
  if(err){
    return 8;
  }

  err = acc_write_byte(ACC_CTRL_REG5, (1<<2));
  if(err){
    return 8;
  }

  err = acc_write_byte(ACC_CTRL_REG1, (1<<0) | (1<<1) | (1<<4) | (1<<5) | (1<<7));
  if(err){
    return 8;
  }

  I2C_0_uninit();
	
	return 9;
}
