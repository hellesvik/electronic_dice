#ifndef ACCELEROMETER_H_
#define ACCELEROMETER_H_

#define INT_EN_ASLP_bm (1<<7)
#define INT_EN_FF_MT_bm (1<<2)

#define INT1_PORT PORTC
#define INT1_PIN_bm (1<<1)

#define INT2_PORT PORTA
#define INT2_PIN_bm (1<<1)

#define ACC_STATUS        0x00
#define ACC_OUT_X_MSB     0x01
#define ACC_OUT_X_LSB     0x02
#define ACC_OUT_Y_MSB     0x03
#define ACC_OUT_Y_LSB     0x04
#define ACC_OUT_Z_MSB     0x05
#define ACC_OUT_Z_LSB     0x06
#define ACC_SYSMOD        0x0B
#define ACC_INT_SOURCE    0x0C
#define ACC_WHO_AM_I      0x0D
#define ACC_XYZ_DATA_CFG  0x0E
#define ACC_PL_STATUS     0x10
#define ACC_PL_CFG        0x11
#define ACC_PL_COUNT      0x12
#define ACC_PL_BF_ZCOMP   0x13
#define ACC_PL_THS_REG    0x14
#define ACC_FF_MT_CFG     0x15
#define ACC_FF_MT_SRC     0x16
#define ACC_FF_MT_THS     0x17
#define ACC_FF_MT_COUNT   0x18
#define ACC_ASLP_COUNT    0x29
#define ACC_CTRL_REG1     0x2A
#define ACC_CTRL_REG2     0x2B
#define ACC_CTRL_REG3     0x2C
#define ACC_CTRL_REG4     0x2D
#define ACC_CTRL_REG5     0x2E
#define ACC_OFF_X         0x2F
#define ACC_OFF_Y         0x30
#define ACC_OFF_Z         0x31

#define EXPECTED_ADDR     0x5A

int8_t acc_enable();
void acc_roll();

#endif /* ACCELEROMETER_H_ */
