#ifndef F_CPU
#define F_CPU 20000000
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <avr/cpufunc.h>

#include <util/delay.h>
#include <string.h>

#include "segments_control.h"
#include "accelerometer.h"

#include <avr/sleep.h>

static uint8_t is_roll = 0;

void interrupt_pins_init(){
  INT1_PORT.DIRCLR = INT1_PIN_bm;
  INT2_PORT.DIRCLR = INT2_PIN_bm;
  INT1_PORT.PIN1CTRL = (1<<7) | (1<<0);
  INT2_PORT.PIN1CTRL = (1<<7) | (1<<0);
}

void interrupt_pins_uninit(){
  INT1_PORT.PIN1CTRL = 0x00;
  INT2_PORT.PIN1CTRL = 0x00;
}

void sleep_start(){

  PORTA.DIR = 0x00; 
  PORTA.PIN0CTRL = PORT_PULLUPEN_bm; 
  PORTA.PIN1CTRL = PORT_PULLUPEN_bm; 
  PORTA.PIN2CTRL = PORT_PULLUPEN_bm; 
  PORTA.PIN3CTRL = PORT_PULLUPEN_bm; 
  PORTA.PIN4CTRL = PORT_PULLUPEN_bm; 
  PORTA.PIN5CTRL = PORT_PULLUPEN_bm; 
  PORTA.PIN6CTRL = PORT_PULLUPEN_bm; 
  PORTA.PIN7CTRL = PORT_PULLUPEN_bm; 

  PORTB.DIR = 0x00; 
  PORTB.PIN0CTRL = PORT_PULLUPEN_bm; 
  PORTB.PIN1CTRL = PORT_PULLUPEN_bm; 
  PORTB.PIN2CTRL = PORT_PULLUPEN_bm; 
  PORTB.PIN3CTRL = PORT_PULLUPEN_bm; 
  PORTB.PIN4CTRL = PORT_PULLUPEN_bm; 
  PORTB.PIN5CTRL = PORT_PULLUPEN_bm; 
  PORTB.PIN6CTRL = PORT_PULLUPEN_bm; 
  PORTB.PIN7CTRL = PORT_PULLUPEN_bm; 

  PORTC.DIR = 0x00; 
  PORTC.PIN0CTRL = PORT_PULLUPEN_bm; 
  PORTC.PIN1CTRL = PORT_PULLUPEN_bm; 
  PORTC.PIN2CTRL = PORT_PULLUPEN_bm; 
  PORTC.PIN3CTRL = PORT_PULLUPEN_bm; 
  PORTC.PIN4CTRL = PORT_PULLUPEN_bm; 
  PORTC.PIN5CTRL = PORT_PULLUPEN_bm; 

  TCA0.SINGLE.CTRLA = 0x00;

  interrupt_pins_init();
  segments_timer_uninit();
  //cli();
  sleep_enable();
  sleep_cpu();
}

void sleep_stop(){
  sleep_disable();
  segments_timer_init();
  segments_leds_init();
}

void wait_for_roll(){
  acc_roll();
  sleep_start();
  sleep_stop();
}

int main(void)
{
  ccp_write_io((uint8_t*)&CLKCTRL.MCLKCTRLB,0);
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);

  _delay_ms(30);

  segments_leds_init();
  segments_timer_init();
  interrupt_pins_init();

  uint8_t ret =  acc_enable();

  sei();	

  _delay_ms(10);
  dice_clear();
  _delay_ms(5);

  while (1) {
    wait_for_roll();
  }

}

ISR(PORTA_PORT_vect) { //INT2
  is_roll = 1;
  INT2_PORT.INTFLAGS = 0xff;
}

ISR(PORTC_PORT_vect) { //INT1
  is_roll = 1;
  INT1_PORT.INTFLAGS = 0xff;
}
