/*
 * dice_firmware.c
 *
 * Created: 4/8/2022 10:38:46 PM
 * Author : medlem
 */ 

#ifndef F_CPU
#define F_CPU 20000000
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <avr/cpufunc.h>
#include <avr/sleep.h>

#include <util/delay.h>

int main(void)
{
    ccp_write_io((uint8_t*)&CLKCTRL.MCLKCTRLB,0);
    _delay_ms(30);

    SLPCTRL.CTRLA |= SLPCTRL_SMODE_PDOWN_gc;
    SLPCTRL.CTRLA |= SLPCTRL_SEN_bm;

    sleep_cpu();
}







